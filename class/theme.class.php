<?php


class WP_Theme
{
    public function __construct()
	{
		add_theme_support( 'post-thumbnails' );

        $this->sThemeURL = get_bloginfo('stylesheet_directory');

        if( !is_admin() )
            add_action( 'wp_enqueue_scripts', array( $this, 'loadPublicScripts' ) );

	}

    function loadPublicScripts()
    {
        wp_enqueue_script('theme-public', $this->sThemeURL.'/js/libs/modernizr-2.0.6.min.js', array( 'jquery' ));
    }
}


?>